#ifndef BITMAP_H
#define BITMAP_H

#include "types.h"

#define BITMAP_WIDTH 16
#define BITMAP_HEIGHT 16

extern const uint16 bitmap[];

#endif
