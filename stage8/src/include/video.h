#ifndef SPACE_INVADERS_VIDEOBASE
#define SPACE_INVADERS_VIDEOBASE

#include "types.h"

uint16* get_video_base();
void set_video_base(uint16* base);

#endif /* !1SPACE_INVADERS_VIDEOBASE */